import React from 'react';
import './komponen.css';

class KomponenInput extends React.Component {
    render() {
        return (
            <div className="komponen-input">
                <input type="text" name='nama' placeholder="User Name" />
                <input type="password" name='password' placeholder="Password" />
                <button type="submit">Log in</button>
            </div>
        );
    }
}

export default KomponenInput;